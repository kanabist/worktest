<?

function clean($value = "") {
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    
    return $value;
}

$data = null;

foreach ($_POST as $key => $value) {
	$data[$key] =  clean($value);
}

if(!empty($data['name']) &&  !empty($data['email']) && !empty($data['phone']) && !empty($data['date']) && !empty($data['url']) && !empty($data['msg'])) {

	$data['phone'] = preg_replace("/[^0-9+]/", '', $data['phone']);

	$date = new DateTime($data['date']);
	$date->add(new DateInterval('P15D'));

	$data['end_date'] = $date->format('Y-m-d');


print_r($data); //debug

}

?>