<?php

class Aidalab_Cdata_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
    	$token = $this->getRequest()->getParam('token');
        if(!$this->_getHelper()->validateToken($token)) throw new Exception("Cannot validate token");

        $collection = Mage::getModel("customer/customer")
            ->getCollection()
            ->addAttributeToSelect('*');

        $result = array();

        foreach ($collection as $i => $customer) {
            /** @var $customer Mage_Customer_Model_Customer */
            $result[$i]['login'] = $customer->getEmail();
            $result[$i]['firstname'] = $customer->getFirstname();
            $result[$i]['lastname'] = $customer->getLastname();
        }

        echo $this->_getHelper()->jsonEncode($result);
    }

    /**
     * @return Aidalab_Cdata_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("cdata");
    }
}
