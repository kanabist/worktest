<?php

class Aidalab_Cdata_ViewController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $token = $this->getRequest()->getParam('token');
        $id = $this->getRequest()->getParam('id');
        if(!$this->_getHelper()->validateToken($token) || !$id) throw new Exception("Cannot validate data");
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::getModel("customer/customer")->load($id);
    	var_dump($customer->getData());
    }
    /**
     * @return Aidalab_Cdata_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("cdata");
    }
}
