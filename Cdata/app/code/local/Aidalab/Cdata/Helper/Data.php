<?php
class Aidalab_Cdata_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * @return string|bool
     */
    protected function _getToken() {
        return Mage::getStoreConfig("admin/cdata/token");
    }

    /**
     * @param string $token
     * @return bool
     */
    public function validateToken($token) {
        return $token == $this->_getToken();
    }
}