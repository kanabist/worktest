<?

function iserror($xml){
    if (!$xml) {
        echo "Failed loading XML\n";
        foreach(libxml_get_errors() as $error) {
                echo "\t", $error->message;
        }
    }
}

function currency($xml, $name){
    $cur = $xml->xpath('//*[@currency="'.$name.'"]')[0]['rate'];
    return $cur;
}

$posturl = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
$curl = curl_init($posturl);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$result = curl_exec($curl);
curl_close($curl);

if(empty($result)){
	echo 'Ошибка получения данных';
}
else{
    $xml = simplexml_load_string($result);
    iserror($xml);

    $USD = currency($xml, 'USD');
    $GBP = currency($xml, 'GBP');
	var_dump($USD,$GBP); //debug
}

?>

